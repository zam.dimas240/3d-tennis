import { useLoader } from "@react-three/fiber";
import { useRef, useEffect, useState } from "react";
import { TextureLoader } from "three/src/loaders/TextureLoader";
import { useSphere, useContactMaterial } from '@react-three/cannon';
import useSound from 'use-sound';
import texture from '../assets/texture.jpg';
import zvuk from '../assets/zvuk.mp3';
import { Howl, Howler } from 'howler';
import { useFrame } from '@react-three/fiber';

export default function Sphere({position, gameOver}) {
  useContactMaterial('ring', 'bouncy', {
    restitution: 0.45
  })
  const textureMap = useLoader(TextureLoader, texture);
  const audioRef = useRef();
  const playZvuk = () => { if (audioRef.current) audioRef.current.play(); };
  const [isOffScreen, setIsOffScreen] = useState(false);
  useEffect(() => {
    audioRef.current = new Howl({
      src: [zvuk]
    });
  }, []);

  useEffect(() => {
    playZvuk();
  }, [audioRef]);

  const [ref, api] = useSphere(() => ({
    mass: 1,
    impactVelocity: 1,
    restitution: 30,
    rotation: [-Math.PI / 2, 0, 0],
    material: 'bouncy',
    onCollide: () => playZvuk(),
    position,
  }));

  useFrame(() => {
    api.position.subscribe(position => {
      if (position[1] < -15) {
        gameOver();
      }
    });
  });

  return (
    <mesh 
      ref={ref}
      scale={0.65}
      castShadow
    >
      <sphereGeometry />
      <meshStandardMaterial color="orange" />
    </mesh>
  );
}
