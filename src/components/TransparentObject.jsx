import { useBox } from '@react-three/cannon';
import { useState, useEffect } from 'react';

export default function TransparentObject({ addCounter }) {

  const generateRandomArray = () => {
    var firstElement = getRandomInt(-13, 13);
    var secondElement = getRandomInt(1, 8);
    var thirdElement = 0;
    var array = [firstElement, secondElement, thirdElement];
    return array;
}

const getRandomInt = (min, max) => {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

  const [ref, api] = useBox(() => ({ 
    mass: 0,
    type: 'Static',
    args: [0, 0, 0],
    position: generateRandomArray(),
    onCollide: () => {
      onColluideFunc()
      addCounter();
    }
  }));

  const onColluideFunc = () => {
    const random = generateRandomArray();
    api.position.set(random[0], random[1], random[2]);
  }

  return (
    <mesh ref={ref}>
      <boxGeometry args={[1, 1, 1]} />
      <meshBasicMaterial transparent opacity={0.5} />
    </mesh>
  );
}


