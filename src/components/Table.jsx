import React, { useRef, useEffect, useState } from 'react'
import { useGLTF } from '@react-three/drei'
import { useFrame } from '@react-three/fiber';
import { useBox, useContactMaterial } from '@react-three/cannon';
import zvuk from '../assets/zvuk.mp3';

export default function Model(props) {
  useContactMaterial('ring', 'bouncy', {
    restitution: 0.7
  })
  const { nodes, materials } = useGLTF('/table/scene.gltf');
  const [group, api] = useBox(() => ({
    type: 'Static',
    mass: 0, 
    rotation: [-Math.PI / 1, -Math.PI / 1, -Math.PI / 1],
    args: [4, 1 / 2, 3],
    restitution: 5,
    material: 'ring',
  }));
const mouse = useRef([0, 0]);

useEffect(() => {
  const onMouseMove = (event) => {
  const { clientX, clientY } = event;
  mouse.current = [clientX - window.innerWidth / 2, -(clientY - window.innerHeight / 2)];
};

window.addEventListener('mousemove', onMouseMove);
return () => window.removeEventListener('mousemove', onMouseMove);
}, []);

useFrame(() => {
  const [x, y] = mouse.current;
  const xEv = x / 40;
  const yEv = y / 40;
  api.rotation.subscribe(rotation => {
    if (xEv > 0) {
      if (rotation[0] > xEv) {
        api.rotation.set(-Math.PI / 1, -Math.PI / 1, xEv / 24);
      } else {
        api.rotation.set(-Math.PI / 1, -Math.PI / 1, xEv / 24);
      }
    } else if (xEv < 0) {
      if (rotation[0] < xEv) {
        api.rotation.set(-Math.PI / 1, -Math.PI / 1, xEv / 24);
      } else {
        api.rotation.set(-Math.PI / 1, -Math.PI / 1, xEv / 24);
      }
    }
  });
  api.position.set(xEv, yEv, 0);
});

  return (
    <mesh castShadow ref={group} {...props}>
      <group dispose={null}>
        <group rotation={[-Math.PI / 2, 0, 0]}>
          <group rotation={[Math.PI / 2, 0, 0]}>
            <group scale={0.29}>
              <mesh castShadow receiveShadow geometry={nodes.Object_5.geometry} material={materials.Material_0} />
              <mesh castShadow receiveShadow geometry={nodes.Object_6.geometry} material={materials['Material.001']} />
              <mesh castShadow receiveShadow geometry={nodes.Object_7.geometry} material={materials.Material} />
            </group>
          </group>
        </group>
      </group>
    </mesh>
  )
}

useGLTF.preload('/scene.gltf')
