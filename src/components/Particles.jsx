import { useRef, useEffect } from 'react';
import { useFrame } from '@react-three/fiber';

export default function Particles() { 
  const particlesRef = useRef([]);
  const mouse = useRef([0, 0]);

  useEffect(() => {
    const onMouseMove = (event) => {
      const { clientX, clientY } = event;
      mouse.current = [clientX - window.innerWidth / 2, -(clientY - window.innerHeight / 2)];
    };

    document.addEventListener('mousemove', onMouseMove);

    return () => {
      document.removeEventListener('mousemove', onMouseMove);
    };
  }, []);

  useFrame(() => {
    particlesRef.current.forEach((particle) => {
      const [x, y] = mouse.current; // Получение положения курсора мыши
      particle.position.x = x;
      particle.position.y = y;
    });
  });

  // Создание частиц
  const numParticles = 1000;
  const particles = [];
  for (let i = 0; i < numParticles; i++) {
    particles.push(
      <mesh key={i} ref={(ref) => (particlesRef.current[i] = ref)}>
        <sphereGeometry args={[0.1, 32, 32]} />
        <meshStandardMaterial color="blue" />
      </mesh>
    );
  }

  return <>{particles}</>;
}