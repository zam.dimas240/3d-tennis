import { useLoader } from "@react-three/fiber";
import { useRef } from "react";
import { TextureLoader } from "three/src/loaders/TextureLoader";
import { usePlane } from '@react-three/cannon';
import texture from '../assets/texture.jpg';
import { DoubleSide } from "three";

export default function Plane(props) {
  const textureMap = useLoader(TextureLoader, texture);
  const [ref] = usePlane(() => ({ rotation: [-Math.PI / 2, 0, 0], ...props }));
  return (
    <mesh 
      ref={ref}
    >
      <planeGeometry args={[100, 100]} />
      <meshStandardMaterial map={textureMap} side={DoubleSide} />
    </mesh>
  );
}