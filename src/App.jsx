import { Canvas, useLoader } from "@react-three/fiber";
import Sphere from "./components/Sphere";
import Plane from "./components/Plane";
import Table from "./components/Table";
import TransparentObject from "./components/TransparentObject";

import "./App.css";
import { DoubleSide, TextureLoader } from "three";
import "./App.css";
import texture from './assets/soulTexture.jpg';
import { Physics, usePlane } from '@react-three/cannon';
import { Suspense, useRef } from "react";
import { useState } from "react";

function App() {
  const textureMap = useLoader(TextureLoader, texture);
  const [isGameOver, setGameOver] = useState(false);
  const [isNewGame, setNewGame] = useState(true);
  const [counter, setCounter] = useState(0);

  const newGame = () => {
    setGameOver(false)
    setCounter(0);
  }

  const addCounter = () => {
    setCounter((prev) => prev + 1);
  }

  return (
    <div className="App">
      {
        isNewGame ? (
          <div className="gameOver__container" onClick={() => setNewGame(false)}> 
            <h1 className="gameOver__title">Начало игры</h1>
            <h2 className="gameOver__title">Кликни что бы начать игру</h2>
          </div>
        ) : !isGameOver ? (
        <div> 
          <div className="game">
            <h1 className="game__counter">{counter}</h1>
          </div>
        <Canvas
        shadows
        camera={{
          fov: 20,
          position: [0, 20, 50],
        }}
      >
        <mesh rotation={[-Math.PI / 1, 0, -30]} position={[10, 5, -30]} receiveShadow>
          <planeGeometry args={[100, 100]} />
          <meshStandardMaterial side={DoubleSide} />
        </mesh>
        <Physics
          gravity={[0, -18, 0]}
        >
          <ambientLight intensity={1} color="white" />
          <directionalLight
            position={[1, 5, 10]}
            intensity={0.7} 
            castShadow
            shadow-mapSize={[2048, 2048]}
            shadow-radius={20}
          >
            <orthographicCamera attach="shadow-camera" args={[-100, 100, -200, 20, 0.5, 60]} />
          </directionalLight>
          <Suspense>
            <Table position={[30, 10, 0]} />
            <Sphere position={[0, 10, 0]} gameOver={() => setGameOver(true)} />
            <TransparentObject counter={counter} addCounter={() => addCounter()} />
          </Suspense>
        </Physics>
      </Canvas>
      </div>
        ) : (
          <div className="gameOver__container" onClick={() => newGame()}> 
            <h1 className="gameOver__title">GAME OVER</h1>
            <h2 className="gameOver__title">Кликни что бы начать заново</h2>
            <h3 className="gameOver__title">Очков: {counter}</h3>
          </div>
        )
      }
    </div>
  );
}

export default App;